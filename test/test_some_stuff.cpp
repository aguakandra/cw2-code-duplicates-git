#include "AbstractCodeSource.h"
#include "Date.h"
#include "Util.h"
#include <iostream>
#include <sstream>
#define BOOST_TEST_MODULE Main
#include <boost/test/unit_test.hpp>
using namespace std;

// This test suite is to validate functionality
BOOST_AUTO_TEST_SUITE( behavior )
BOOST_AUTO_TEST_CASE( number_source_test_size ) {
	NumberCodeSource cs(1,10);
	BOOST_CHECK_EQUAL( cs.size(), 10 );
}

BOOST_AUTO_TEST_CASE( date_source_size ) {
	DateCodeSource reg(Date(2011,1,1), Date(2011,12,31));
	BOOST_CHECK_EQUAL( reg.size(), 365 );
	DateCodeSource leapyear(Date(2012,1,1), Date(2012,12,31));
	BOOST_CHECK_EQUAL( leapyear.size(), 366 );
}

BOOST_AUTO_TEST_CASE( word_list_size ) {
	vector<string> words;
	words.push_back("fee");
	words.push_back("fie");
	words.push_back("foe");
	words.push_back("fum");
	WordList w = WordList(words);
	BOOST_CHECK_EQUAL(w.length, 4);
	BOOST_CHECK_EQUAL(w.width, 3);
	BOOST_CHECK_EQUAL(w[3], "fum");
}

BOOST_AUTO_TEST_CASE( three_word_use ) {
	vector<string> words;
	words.push_back("fee");
	words.push_back("fie");
	words.push_back("foe");
	words.push_back("fum");
	WordList w = WordList(words);
	ThreeWordCodeSource cs(w);
	BOOST_CHECK(cs.hasNext());
	BOOST_CHECK_EQUAL(cs.size(), 4*4*4);
}
BOOST_AUTO_TEST_CASE( greek_display_with_ptrs ){
	stringstream ss;
	PresetGreekCodeSource cs;
	ss << cs;
	string raw_name = ss.str();
	BOOST_CHECK(!raw_name.empty());
	ss.str("");
	ThreeWordCodeSource *p1 = &cs;
	ss << *p1;
	string three_name = ss.str();
	BOOST_CHECK(!three_name.empty());
	BOOST_CHECK_EQUAL(raw_name, three_name);
	ss.str("");
	AbstractCodeSource *p2 = &cs;
	ss << *p2;
	string abstract_name = ss.str();
	BOOST_CHECK(!abstract_name.empty());
	BOOST_CHECK_EQUAL(raw_name, abstract_name);
}
BOOST_AUTO_TEST_CASE( greeks_cross_dicts ) {
	vector<WordList> lists;
	lists.push_back(ThreeWordCodeSource::GREEK_WORDS);
	lists.push_back(ThreeWordCodeSource::RANDOM_WORDS);
	vector<string> separators(3, "");
	separators[1] = " ";
	MultiWordCodeSource cs(lists, separators);
	BOOST_CHECK(cs.size() == lists[0].length * lists[1].length);
}
BOOST_AUTO_TEST_CASE( code_list_size ) {
	CodeSourceList l;
	l.addSource(new PresetGreekCodeSource());
	l.addSource(new PresetDictCodeSource());
	{
		NumberCodeSource ncs(1,10);
		l.addSourceCopy(&ncs);
	}
	const bigcount_t sz = l.size();
	const bigcount_t expected_sz = 24*24*24 + 100*100*100 + 10;
	//cout << l << "\n\n";
	BOOST_CHECK_EQUAL(sz, expected_sz);
}
BOOST_AUTO_TEST_CASE( seq_size ) {
	SequentialLettersCodeSource cs(3);
	size_t i = 0;
	string code;
	while (cs.hasNext()) {
		code = cs.nextCode();
		if (i == 0) BOOST_CHECK_EQUAL(code, "a");
		i++;
	}
	BOOST_CHECK_EQUAL(i, cs.size());
	BOOST_CHECK_EQUAL(i, 26+26*26+26*26*26);
	BOOST_CHECK_EQUAL(code, "zzz");
}
BOOST_AUTO_TEST_SUITE_END()

// This test suite is to see what happens
BOOST_AUTO_TEST_SUITE( display )
BOOST_AUTO_TEST_CASE( number_source_display ) {
	NumberCodeSource cs(1, 10);
	cout << cs << "[";
	while (cs.hasNext()) cout << " " << cs.nextCode();
	cout << "]\n\n";
}
BOOST_AUTO_TEST_CASE( preset_display ){
	cout << "Preset greek object: ";
	PresetGreekCodeSource cs;
	cout << cs << "\n";
	cout << "Preset dict object: " << PresetDictCodeSource() << "\n\n";
}
BOOST_AUTO_TEST_CASE( one_multi_word ) {
	static const char* word_array[] = {"a", "b", "c"};
	vector<string> words(word_array, word_array + 3);
	WordList list(words);
	BOOST_REQUIRE_EQUAL(list.length, 3);
	MultiWordCodeSource cs;
	cs.addWord(words,"");
	cs.finalize("");
	BOOST_REQUIRE_EQUAL(cs.size(), 3);
	static const char* expected[] = {
		"a", "b", "c"
	};
	cout << cs << "\nCodes =";
	for (int i = 0; i < 3; i++) {
		BOOST_REQUIRE(cs.hasNext());
		string code = cs.nextCode();
		cout << " " << code;
		BOOST_CHECK_EQUAL(expected[i], code);
	}
	cout << "\n\n";
}

BOOST_AUTO_TEST_CASE( two_multi_word ) {
	static const char* word_array[] = {"a", "b", "c"};
	vector<string> words(word_array, word_array + 3);
	WordList list(words);
	BOOST_REQUIRE_EQUAL(list.length, 3);
	MultiWordCodeSource cs;
	cs.addWord(words,"[");
	cs.addWord(words,"-");
	cs.finalize("]");
	BOOST_REQUIRE_EQUAL(cs.size(), 3*3);
	static const char* expected[] = {
		"[a-a]", "[b-a]", "[c-a]",
		"[a-b]", "[b-b]", "[c-b]",
		"[a-c]", "[b-c]", "[c-c]"
	};
	cout << cs << "\nCodes =";
	for (int i = 0; i < 3*3; i++) {
		BOOST_REQUIRE(cs.hasNext());
		string code = cs.nextCode();
		cout << " " << code;
		BOOST_CHECK_EQUAL(expected[i], code);
	}
	cout << "\n\n";
}
BOOST_AUTO_TEST_CASE( file_read ) {
	const char* filename = "../input/5animals.txt";
	ifstream fin;
	fin.open(filename);
	BOOST_REQUIRE(fin);
	WordList list = WordList::readFile(fin);
	BOOST_REQUIRE(list.length > 0);
	cout << "Words:";
	for (size_t i = 0; i < list.length; i++)
		cout << " " << list[i];
	cout << "\n\n";
	fin.close();
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( util )

void test_byte_conv(bigcount_t bytes, string expected) {
	stringstream ss;
	string result;
	displayByteSize(ss, bytes);
	result = ss.str();
	BOOST_CHECK_EQUAL(result, expected);
}
BOOST_AUTO_TEST_CASE( byte_conversion) {
	test_byte_conv(0, "0 bytes");
	test_byte_conv(1, "1 bytes");
	test_byte_conv(10, "10 bytes");
	test_byte_conv(100, "100 bytes");
	test_byte_conv(1000, "1000 bytes");
	test_byte_conv(2047, "2047 bytes");
	test_byte_conv(2048, "2048 bytes");
	test_byte_conv(2049, "2 kilobytes");
	test_byte_conv(300*1024, "300 kilobytes");
	test_byte_conv(2048*1024, "2048 kilobytes");
	test_byte_conv(2049*1024, "2 megabytes");
	test_byte_conv(300*1024*1024, "300 megabytes");
	test_byte_conv(2048ULL*1024*1024, "2048 megabytes");
	test_byte_conv(2049ULL*1024*1024, "2 gigabytes");
	test_byte_conv(300ULL*1024*1024*1024, "300 gigabytes");
	test_byte_conv(2048ULL*1024*1024*1024, "2048 gigabytes");
	test_byte_conv(2049ULL*1024*1024*1024, "2049 gigabytes");
}

BOOST_AUTO_TEST_SUITE_END()