# Creeper World 2 Code Maps: Duplicate Code Search

The purpose of this project is to explore the mapping between Code Names (text) and Seeds (the hashed random number seed). It looks through a bunch of possible code names and finds duplicates. Alternatively, it can look through codes until a code for every possible seed is found.

# Description of Code/Seed Conversion
See [my website](http://aguakandra.com/CW2_codes.php) for details.

# Project Status
There is one main "project" in the Visual Studio "solution": find_duplicates. This compiles to an executable that accepts code sources on the command line and finds duplicates among them.
There are some helper test/benchmark projects which are not as important.

Hidden in the history is another Searcher project which looked through codes until it found one seed for every code.
It is not as important now since I already ran it and got the data (all ~10GB of it).

# Dependencies
* [boost](http://www.boost.org) libraries
  time and program_options compiled, a bunch of other stuff just included as header files.
* (optional) Cygwin + gzip + mv for Searcher
  The code recorder can optionally split the result file into parts, gzip each, and move to another location. I used this since I didn't have enough local space to store everything.

# Command Line Options
Specify code sources on the command line for the duplicate finder executable. The output looks something like this:

	find_duplicates.exe v0.1 (6/12/2013)
	Created by Paul Holcomb (aguakandra@gmail.com)

	Search through CW2 codes for duplicates (two codes that hash to the same seed)

	Allowed options:
	  -h [ --help ]                         Show this help message.

	  -f [ --word-file ] arg                Use words from file [arg], one word per
	                                        line. Can be specified multiple times.

	  -o [ --output-file ] arg (=duplicates.txt)
	                                        Write final output to this file.

	  -u [ --update ] arg (=20)             Show status updates every [arg]
	                                        seconds.

	  -r [ --records ] arg (=0)             Split temp files after [arg] records.
	                                        0 means never split.


	Code Sources:
	  -p [ --presets ]      Use the codes generated by the Random Mission button
	                        (three words from a list of 100, three greek letters,
	                        or a number 0-9999).

	  -d [ --dates ]        Use codes generated by Today's Mission button (all
	                        dates from Jan 1, 0 to Dec 31, 9999).

	  -l [ --letters ] arg  Combinations of up to [arg] lowercase letters.

	  -w [ --word ] arg     A phrase formed from one or more words, from one or
	                        more word files. This can be any string, where numbers
	                        will be replaced by a word from the respective word
	                        list.

	                        Number can be in the range 1-N, where N is the number
	                        of word files specified, and can be wrapped in
	                        parentheses for clarity.

	                        Examples:
	                        -w 123 concatenates a word from lists 1, 2, and 3.
	                        -w "1 1" joins two words from the first word file using
	                                 a space.
	                        -w "The (5) of (4)s" inserts words into a string like
	                                             The Two of Clubs.


	Codes are generated from -r, -d, -l, and -w options in the order given. Only -w
	can be specified more than once.
	At least one code source option is required.
	For example,
	-r -l 4        searches all preset codes, then from a to zzzz
	-w 1 -w "2 2"  searches single words in file 1, then two-word phrases in file 2


# Plans for Future
* Maybe enable the duplicate finder to save and restore state.
* Optimize the crap out of it (see below)
* Designated exe for "find a code which hashes to this code".
* Maybe bring out the Searcher reverse-mapping code

## The problem with planning...
These are inherently custom actions, since there is a ton of postprocessing I did.
Especially for the reverse mapping, I did a merge, then bash sort, then pack into a custom format. Duplicates were processed by sorting and selecting interesting ones.

Oh well. Program provided as-is.

# Future Optimizations
The search process, while it uses a worker thread, only really uses one core at a time. There are huge opportunities for parallelization (with shared search space). This was because I was only running on a dual-core machine and used the second core occasionally for gzipping.

Another option is running this on a more powerful computer or group of computers somewhere, which would be awesome but a lot of work. Especially since I already created the reverse mapping table.

For best performance, compile in release mode and with optimizations turned on.