# Results of Duplicate Search #
This lists some interesting duplicates I found through this process. (This is MarkDown btw)

## Seeds that have duplicates in greek words and random words ##
(115 many, in decimal)
54420340, 69118375, 99809291, 102991866, 162664605, 186553636, 188628602, 202021510, 211204089, 228547953, 283418431, 327935799, 329641841, 378278714, 516570149, 566422987, 655960183, 670315353, 707375189, 806791475, 836673527, 840544991, 895144823, 1019724370, 1045060014, 1061285186, 1092279874, 1102726723, 1114055784, 1163928348, 1169291066, 1190323270, 1221396439, 1254888819, 1258084832, 1287570854, 1327436810, 1344747909, 1361948887, 1370064315, 1380992950, 1389551911, 1496484544, 1506413068, 1509489681, 1541689100, 1585808540, 1596050536, 1659164013, 1690978046, 1700117810, 1765087381, 1768132086, 1807139747, 1824398890, 1894831651, 1933313940, 1976545661, 1996298810, 2076107031, 2098761872, 2117079052, 2132201870, 2132904677, 2203560767, 2266493636, 2273712474, 2318427286, 2426863990, 2451642560, 2456946994, 2564555435, 2577619899, 2619516387, 2678029188, 2689370703, 2773411642, 2851176182, 2878936292, 2911934254, 2967922378, 3021141296, 3021370958, 3029185497, 3036959039, 3084641777, 3090570273, 3159070109, 3164019864, 3223435185, 3289590412, 3313347694, 3314720312, 3321849060, 3331396977, 3344430800, 3391636285, 3460539725, 3489169802, 3497964842, 3529389074, 3652247455, 3703551582, 3722068449, 3854124578, 3871171992, 3874354521, 3925721191, 4054611743, 4095437787, 4113333105, 4157924607, 4166079124, 4188731072, 4201038128

## Best texts
come party round:garden then birthday  
garden a game:the chair said  
the car time:make big play  
play and toy:away from it  
when bell street:boat fly down  
funny snow garden:play again box  
of box jump:can make funny  
for some apple:think were apple  

## Dates
### Same Date
December 3, 9416:December 3, 4264  
January 8, 9601:January 8, 540  
April 29, 2830:April 29, 1027  *use this*  

### Smallest difference between
November 6, 2923:April 13, 2925

### Largest difference between
December 6, 1510:February 25, 9885  

### first noticable overlap (counting past)
September 22, 1992:January 11, 2032

### first noticible overlap (not counting past)
August 3, 2046:August 17, 2191

## 1 word overlaps
07ef278c:waterfowler:bassoon's  
6a38d170:prejudges:filariid  
99f781a0:sketchable:calash's  
c469fbcd:btry:amoebas  
c9eccd9d:gastropod:cousinages  
caa9dcd6:woodruff:inaccuracies  

## Interesting Triplicates
fundamentally obscure	responses pass	essentially managing   
problems object	isn't seeking	introduce numbers  
absolute beautiful	woman foot's	posts statistics  
elsewhere lunch	cases largely	brand existence  
have filled	eight secure	city excuse  
agreement hand	unite tonight	script flies  
succeed freely	machines reports	leaving allowed  

## Interesting Duplicates
difficulties designing	painful data  
together painful	briefly justification  
reduced displays	no binary  
dead eating	defines nice  
idea university	don't complaining  
another remarks	ours Creeper  
Creeper submitted	suspending weather  
Virgil fastest	satisfy owners  
Virgil requires:accuracy memory  
sorry measures:Creeper happening:  
van suspended:vaguely quiet  
dying thereby:doubt cyanide  
chairman attack:invents crater:  

## List of all triplicates from words.10^2
file's term	calls trusted	repeats gradually  
top passes	knows positive	master existence  
demand layout	security points	boat irritates  
lesser covers	losing suggestion	deleting chairman  
fundamentally obscure	responses pass	essentially managing  
sticking french	consumption nearby	crisis extensive  
mistook that	sad willing	accidentally leader  
woman foot's	posts statistics	absolute beautiful  
problems object	isn't seeking	introduce numbers  
points returning	wasting running	law distribute  
tape finish	reveals idea	authors basic  
free fields	quickest ultimate	wild behind  
affair promised	wheels ring	language bet  
postmaster lazy	higher repeat	restrict aspect's  
closed cheaper	arrangement's obey	stream afterwards  
lost spreads	meetings world	instant settles  
amuses leave	alternatives officer	trunk star  
distributes ancient	out owner	nor accept  
balance isolated	destroying manual's	drawn grands  
copy's property	tax watches	an easiest  
seem boot	allowed repeat	disaster binding  
executing laws	fills which	represented activities  
cases largely	elsewhere lunch	brand existence  
preferable like's	external protected	forthcoming himself  
ice badly	funny harmful	image's amounts  
choose started	brown update	affecting qualifying  
ring notice	eye ours	unit machine's  
supposed raising	trusted town	graph monitor  
inventing lecture	spending supposed	negative although  
child's bids	leads pocket	four assuring  
attractive easily	whenever indicates	desire direction  
finishing states	off string's	yourself combination's  
comparison frequently	corners shortage	official explicit  
update sufficiently	involved work's	harm access  
figure's editor's	writing searched	pleasing cursor  
have filled	eight secure	city excuse  
worth talked	nor test's	aim naive  
lectures condition	log judge	died binary  
holidays divided	suspending perfect	competition benefits  
mixing decision	obey firmly	neck anybody  
ever linked	crisp wish's	huge information  
regardless light's	versions spotted	early fire  
commission occasion's	worth wider	output exactly  
her passed	considered wanted	utility initially  
agreement hand	unite tonight	script flies  
topic's his	spell option	recommends byte  
succeed freely	machines reports	leaving allowed  
measure easiest	get's existence	cardboard connection's  
spent from	asks till	pointed artificial  
letter's justifying	drew proposal	model elsewhere  
obscure rate's	resourced wheel's	symbol implementing  
grown kind	throwing terribly	dedicated eaten  
performs morning	model otherwise	groups completed  
election lacking	anyway number's	huge calling  
allowed growing	planned preferred	charge's fell  
sequence's closed	over twelve	root automobile  
stands hello	lack than	edit argue  

## Forum Post
I shared these results on [the knucklecracker forum](http://knucklecracker.com/forums/index.php?topic=8853.msg59953#msg59953)
