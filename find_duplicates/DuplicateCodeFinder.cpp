#include "DuplicateCodeFinder.h"
#include "Util.h"
#include "HashAString.h"

#include <sstream>
#include <iostream>
#include <iomanip>
#include <cassert>
#include <cstdio> // for remove
#include <boost/foreach.hpp>
using namespace std;

const string DuplicateCodeFinder::DEFAULT_CODEFILE = "codes_main.tmp";
const string DuplicateCodeFinder::DEFAULT_DUPFILE = "codes_dups.tmp";
const string DuplicateCodeFinder::DEFAULT_RESULTFILE = "duplicates.txt";

DuplicateCodeFinder::DuplicateCodeFinder(const string& codefile, const string& dupfile, size_t recordsPerFile)
	: seeds_seen(), seeds_with_duplicates(), codefile(codefile), dupfile(dupfile),
	codeRecorder(codefile, recordsPerFile),
	duplicateRecorder(dupfile, recordsPerFile)
{
}

/*
Accumulate code/seed pairs. Store everything in the main codeRecorder file,
and store duplicates, etc in the second file. We need to remember all codes
since we don't know which codes will produce duplicates and we might need the
first code later.
*/
void DuplicateCodeFinder::addCode(const seed_t seed, const string& code) {
	if (seeds_seen.haveFoundCodeForSeed(seed)) {
		// already found at least one code with this seed
		seeds_with_duplicates.insert(seed);
		duplicateRecorder.recordCode(code, seed);
	} else {
		// this is the first seen code for this seed
		seeds_seen.markFoundCodeForSeed(seed);
		codeRecorder.recordCode(code, seed);
	}
}

const set<seed_t>& DuplicateCodeFinder::getDuplicateSeeds() const {
	return seeds_with_duplicates;
}

void DuplicateCodeFinder::closeFiles() {
	codeRecorder.close();
	duplicateRecorder.close();
}

// Remove intermediate files
void DuplicateCodeFinder::cleanup() {
	BOOST_FOREACH(string filename, codeRecorder.getFileList()) {
		remove(filename.c_str());
	}
	BOOST_FOREACH(string filename, duplicateRecorder.getFileList()) {
		remove(filename.c_str());
	}
}

/*
Go through the two intermediate files and make a new result file,
listing each seed that is duplicated and all codes corresponding to that seed.
*/
void DuplicateCodeFinder::compileResults(const string& resultfile) {
	cout << "\nReading and combining data files..." << endl;

	ifstream dupFile, mainFile;
	// get file lists
	vector<string> dupFiles(duplicateRecorder.getFileList());
	vector<string> mainFiles(codeRecorder.getFileList());

	string line;
	multimap<seed_t, string> codes_by_seed;

	BOOST_FOREACH(string filename, dupFiles) {
		dupFile.open(filename);
		if (!dupFile) die("can't open " + filename + " for reading");
		// Read all entries from the duplicate file, storing in the map.
		while (getline(dupFile, line, CodeRecorder::LINE_SEP_CHAR)) {
			istringstream ss(line.substr(0, 8));
			seed_t seed=0;
			ss >> hex >> seed;
			string code = line.substr(8);
			codes_by_seed.insert(pair<seed_t, string>(seed, code));
		}
		dupFile.close();
	}

	BOOST_FOREACH(string filename, mainFiles) {
		mainFile.open(filename);
		if (!mainFile) die("can't open " + filename + " for reading");
		// Go through the primary file and add in the original code for all
		// seeds that have duplicates.
		while(getline(mainFile, line, CodeRecorder::LINE_SEP_CHAR)) {
			istringstream ss(line.substr(0, 8));
			seed_t seed=0;
			ss >> hex >> seed;
			if (codes_by_seed.find(seed) != codes_by_seed.end()) {
				// This seed is one that has duplicates
				string code = line.substr(8);
				codes_by_seed.insert(pair<seed_t, string>(seed, code));
			}
		}
		mainFile.close();
	}

	// now write results file
	// Format is like FF001234:Code 1:Duplicate code 2:Maybe even a triplicate
	ofstream resultFile(resultfile.c_str());
	if (!resultFile) die("can't open " + resultfile + " for writing");
	resultFile.fill('0'); // for hex

	seed_t last_seed;
	bool first = true;
	size_t n_seeds = 0;
	for (multimap<seed_t, string>::iterator it = codes_by_seed.begin(); it != codes_by_seed.end(); ++it) {
		const seed_t& seed = (*it).first;
		const string& code = (*it).second;
		seed_t calc_seed = hashAString_preprocess(code.c_str());
		if (calc_seed != seed) {
			cerr << "SEED MISMATCH for code `" << code << "': it said " << hex << seed << " but I got " << calc_seed << dec << endl;
		}
		if (first || seed != last_seed) {
			// A new seed!
			++n_seeds;
			if (!first) resultFile << "\n";
			resultFile << hex << setw(8) << seed;
		}
		resultFile << ":" << code;
		last_seed = seed;
		first = false;
	}
	resultFile.close();
	
	size_t dupn = codes_by_seed.size();
	double dup_ratio = double(dupn) / double(n_seeds);
	cout << "Parsed " << dupn << " duplicate code entries for " << n_seeds << " seeds (ratio = " << dup_ratio << ")\n";
	cout << "Results written to file " << resultfile << "\n\n";
}