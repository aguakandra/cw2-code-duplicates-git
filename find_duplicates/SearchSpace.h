#pragma once
#include <cstdint>
#include <iostream>
#include "Seed.h"
using namespace std;

class SearchSpace
{
public:
	static const bigcount_t SIZE  = 0x100000000U;
	static const size_t BYTE_SIZE = 0x20000000U;

	SearchSpace();
	~SearchSpace();
	bool haveFoundCodeForSeed(const seed_t seed) const;
	void markFoundCodeForSeed(const seed_t seed);

	bigcount_t getNumFoundSeeds() const;
	bool allDone() const;
	
	friend ostream& operator<<(ostream& stream, const SearchSpace& ss);
	friend istream& operator>>(istream& stream, SearchSpace& ss);

private:
	uint8_t* foundCodeForSeedBits;
	bigcount_t numFoundSeeds;
};