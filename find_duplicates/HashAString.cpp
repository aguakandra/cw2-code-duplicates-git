#include "HashAString.h"
#include <cstring>
#include <cctype>
/*
BUG
doesn't lowercase or trim the string
meaning the caller must do that ahead of time

this is only an issue for current usage for the greek letters Alpha Beta etc
which start with an uppercase

solution: deal with greeks in lowercase only
if it is a big deal go back and uppercase them in the data
*/
static char buffer[200];
seed_t hashAString_preprocess(const char *str)
{
	const size_t len = strlen(str);
	char *buf = buffer;
	const char *str_start = str; // first char to copy
	const char *str_end = str_start + len - 1; // last char to copy
	// trim leading whitespace
	while (str_start <= str_end && isspace(*str_start)) {
		++str_start;
	}
	// trim trailing whitespace
	while (str_start <= str_end && isspace(*str_end)) {
		--str_end;
	}
	// copy into buffer
	for (const char *str_p = str_start; str_p <= str_end; ++str_p, ++buf) {
		*buf = tolower(*str_p);
	}
	// set trailing null
	*buf = 0;
	//std::cout << "`" << str << "' -> `" << buffer << "'\n";
	return hashAString(buffer);
}

seed_t hashAString(const char *str)
{
	MD5_CTX ctx;
	MD5_Init(&ctx);
	MD5_Update(&ctx, str, strlen(str));
	unsigned char result[16];
	MD5_Final(result, &ctx);
	return result[0] << 24 | result[1] << 16 | result[2] << 8 | result[3];
}