#include "WordList.h"
#include <boost/make_shared.hpp>
#include <algorithm>
#include <cassert>

using namespace std;
ostream& operator<<(ostream& os, const WordList& w) {
	os << "WordList of " << w.length << " words (width=" << w.width << ")";
	return os;
}
bool lessThanLength(const string& a, const string& b) {
	return a.length() < b.length();
}
// Make a WordList from an existing list of words.
// A defensive copy will be made.
WordList::WordList(const std::vector<std::string>& input_list)
	: data(boost::make_shared<list_type>(input_list)), length(input_list.size()),
	width(length > 0 ? max_element(input_list.begin(), input_list.end(), lessThanLength)->length() : 0)
{
}
// Make a WordList by reading in words from a file stream.
WordList WordList::readFile(std::istream& input_filestream) {
	vector<string> words;
	string temp;
	assert(input_filestream.good());
	while (getline(input_filestream, temp)) {
		words.push_back(temp);
	}
	assert(!words.empty());
	return WordList(words);
}


