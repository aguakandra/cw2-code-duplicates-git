#include "SearchSpace.h"
#include "BitCount.h"
#include "Searcher.h"
#include <cassert>

/** Read and write the SearchSpace to a stream */
ostream& operator<<(ostream& stream, const SearchSpace& ss)
{
	stream.write((const char*) &ss.numFoundSeeds, sizeof(ss.numFoundSeeds));
	stream.write((const char*) ss.foundCodeForSeedBits, ss.BYTE_SIZE);
	cout << "wrote stream state with " << ss.numFoundSeeds << " seeds\n";
	return stream;
}

istream& operator>>(istream& stream, SearchSpace& ss)
{
	size_t claimednumFoundSeeds;
	ss.numFoundSeeds = 0;
	stream.read((char*) &claimednumFoundSeeds, sizeof(size_t));
	stream.read((char*) ss.foundCodeForSeedBits, ss.BYTE_SIZE);

	// check that the number of found codes equals the claim
	ss.numFoundSeeds += countBits(ss.foundCodeForSeedBits, ss.BYTE_SIZE);
	assert(ss.numFoundSeeds == claimednumFoundSeeds);

	cout << "read in stream state with " << ss.numFoundSeeds << " found seeds\n";
	return stream;
}



/** Read and write the search state to a file.
This lets the search be paused and resumed.
Data saved:
- file number (so that even if earlier files are deleted or moved, the code recorder still increments properly)
- # of attempted codes (skip this many codes at the beginning)
*/
void Searcher::writeState(const char * filename)
{
	stop();
	cout << "writing state with " << attemptedCodes << " attempted codes... " << flush;
	ofstream of(filename, ios::out | ios::binary);
	size_t fileNumber = codeRecorder.getFileNumber();
	of.write((char*) &fileNumber, sizeof(unsigned));
	of.write((char*) &attemptedCodes, sizeof(size_t));
	of << searchSpace;
	of.close();
	cout << "wrote state to " << filename << "\n";
}

void Searcher::readState(const char * filename)
{
	stop();
	cout << "reading state... " << flush;
	ifstream in(filename, ios::in | ios::binary);
	size_t numCodes, fileNumber;
	in.read((char*) &fileNumber, sizeof(unsigned));
	codeRecorder.setFileNumber(fileNumber);
	in.read((char*) &numCodes, sizeof(size_t));
	in >> searchSpace;
	cout << "read in data, now checking records to see if only " << numCodes << " codes are found, as claimed\n";
	// get code source back in sync
	// NOTE this assumes that the code source does not change.
	attemptedCodes = 0;
	codeSource.reset();
	while (codeSource.hasNext() && attemptedCodes < numCodes) {
		codeSource.nextCode();
		++attemptedCodes;
	}
	assert(attemptedCodes == numCodes);
	cout << "read state from " << filename << "\n";
}