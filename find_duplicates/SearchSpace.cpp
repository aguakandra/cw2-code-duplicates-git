#include "SearchSpace.h"
#include <cstring>

SearchSpace::SearchSpace()
{
	foundCodeForSeedBits = new uint8_t[BYTE_SIZE];
	memset(foundCodeForSeedBits, 0, BYTE_SIZE);
	numFoundSeeds = 0;
}

SearchSpace::~SearchSpace()
{
	delete [] foundCodeForSeedBits;
}

bool SearchSpace::haveFoundCodeForSeed(seed_t seed) const
{
	const size_t index = seed >> 3;
	const uint8_t bitnum = seed & 7; // 2^3-1, the remainder
	const uint8_t mask = 1U << bitnum;
	return (foundCodeForSeedBits[index] & mask) != 0;
}

void SearchSpace::markFoundCodeForSeed(seed_t seed)
{
	const size_t index = seed >> 3;
	const uint8_t bitnum = seed & 7; // 2^3-1, the remainder
	const uint8_t mask = 1U << bitnum;

	++numFoundSeeds;

	foundCodeForSeedBits[index] |= mask;
}

bigcount_t SearchSpace::getNumFoundSeeds() const
{
	return numFoundSeeds;
}

bool SearchSpace::allDone() const
{
	return numFoundSeeds == SIZE;
}