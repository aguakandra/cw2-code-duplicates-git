#include "AbstractCodeSource.h"
#include <sstream>
#include <cassert>
#include <cmath>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
using namespace std;

/*************************************
                 data 
**************************************/
static const string randomWordArray[] = {
"a",
"and",
"away",
"big",
"blue",
"can",
"come",
"down",
"find",
"for",
"funny",
"go",
"help",
"here",
"in",
"is",
"it",
"jump",
"little",
"look",
"make",
"not",
"one",
"play",
"red",
"run",
"said",
"see",
"the",
"three",
"to",
"two",
"up",
"we",
"where",
"yellow",
"after",
"again",
"as",
"ask",
"by",
"fly",
"from",
"how",
"just",
"know",
"let",
"may",
"of",
"once",
"put",
"round",
"some",
"then",
"think",
"walk",
"were",
"when",
"apple",
"bear",
"bell",
"birthday",
"boat",
"box",
"bread",
"cake",
"car",
"chair",
"coat",
"day",
"door",
"farm",
"floor",
"flower",
"game",
"garden",
"grass",
"ground",
"hill",
"letter",
"morning",
"nest",
"night",
"paper",
"party",
"rain",
"ring",
"shoe",
"snow",
"song",
"street",
"sun",
"table",
"time",
"top",
"toy",
"tree",
"water",
"way",
"window"
};
static const int num_random_words = sizeof(randomWordArray) / sizeof(randomWordArray[0]);

static const string greekWordArray[] = {
"Alpha",
"Beta",
"Gamma",
"Delta",
"Epsilon",
"Zeta",
"Eta",
"Theta",
"Iota",
"Kappa",
"Lambda",
"Mu",
"Nu",
"Xi",
"Omicron",
"Pi",
"Rho",
"Sigma",
"Tau",
"Upsilon",
"Phi",
"Chi",
"Psi",
"Omega"
};
static const int num_greek_words = sizeof(greekWordArray) / sizeof(greekWordArray[0]);




/*************************************
           NumberCodeSource
**************************************/

// Generate numbers from low to high, including both low and high.
NumberCodeSource::NumberCodeSource(int low, int high)
	: low(min(low,high)), high(max(low,high))
{
	reset();
}

bool NumberCodeSource::hasNext() const {
	return current <= high;
}

const string NumberCodeSource::nextCode() {
	return boost::lexical_cast<string>(current++);
}

void NumberCodeSource::reset() {
	current = low;
}

bigcount_t NumberCodeSource::size() const {
	return high - low + 1;
}

size_t NumberCodeSource::width() const {
	return size_t(ceil(log10(high)));
}

ostream& NumberCodeSource::print(ostream& os) const {
	os << "Numbers from " << low << " to " << high << ", size=" << size();
	return os;
}




/*************************************
            DayCodeSouce
**************************************/

// Generate date strings from begin to end, including both begin and end.
DateCodeSource::DateCodeSource(Date begin, Date end)
	: begin(begin), end(end)
{
	reset();
}

bool DateCodeSource::hasNext() const {
	return current_day <= end;
}

const string DateCodeSource::nextCode() {
	const string s = boost::lexical_cast<string>(current_day);
	current_day = current_day.next();
	return s;
}

void DateCodeSource::reset() {
	current_day = begin;
}

bigcount_t DateCodeSource::size() const {
	return Date::days_between(begin, end) + 1;
}

size_t DateCodeSource::width() const {
	// while the dates might not be this long, this is an upper bound.
	return string("September 31, " + boost::lexical_cast<string>(end.year)).length();
}

ostream& DateCodeSource::print(ostream& os) const {
	os << "Dates from " << begin << " to " << end << ", size=" << size();
	return os;
}




/*************************************
       SequentialLettersCodeSource
**************************************/

// Generate letter strings from a-zzzzz (up to max_letters in length)
SequentialLettersCodeSource::SequentialLettersCodeSource(size_t max_letters)
	: MAX_LETTERS(max_letters), letters(MAX_LETTERS+1, 0)
{
	reset();

	// Calculate and cache size
	bigcount_t sum = 0, combos = 26;
	for (size_t i = 0; i < MAX_LETTERS; i++) {
		sum += combos;
		combos *= 26; // for one more letter, 26 times as many combos
	}
	// One formula: a*(a^n-1)/(a-1) for a = 26, but that's less obvious.
	_size = sum;
}

bool SequentialLettersCodeSource::hasNext() const {
	return letters[MAX_LETTERS] == 0; // will be 'a' on overflow carry out
}

const string SequentialLettersCodeSource::nextCode() {
	// make a copy for return - the c_str is to ensure no extra null characters from buffer show up.
	const string ret(letters.c_str());

	// increment by one, propagating carries
	int i = 0;
	while (++letters[i] > 'z') {
		// reset back to start
		letters[i] = 'a';
		// go on to increment next digit
		++i;
		// check for size increase - no further carry
		if (letters[i] == 0) {
			letters[i] = 'a';
			break;
		}
		// otherwise increment and check again in the loop
	}

	return ret;
}

void SequentialLettersCodeSource::reset() {
	fill(letters.begin(), letters.end(), 0);
	letters[0] = 'a';
}

bigcount_t SequentialLettersCodeSource::size() const {
	return _size;
}

size_t SequentialLettersCodeSource::width() const {
	return MAX_LETTERS;
}

ostream& SequentialLettersCodeSource::print(ostream& os) const {
	os << "Sequences of up to " << MAX_LETTERS << " lowercase letters" << ", size=" << size();
	return os;
}




/*************************************
        ThreeWordCodeSource 
**************************************/

WordList ThreeWordCodeSource::RANDOM_WORDS(vector<string>(randomWordArray, randomWordArray+num_random_words));
WordList ThreeWordCodeSource::GREEK_WORDS(vector<string>(greekWordArray, greekWordArray+num_greek_words));

// Make a code source using words from the specified word list.
ThreeWordCodeSource::ThreeWordCodeSource(const WordList& words)
	: words(words), numWords(words.length)
{
	buffer.reserve(words.width*3+2);
	reset();
}

bool ThreeWordCodeSource::hasNext() const {
	return word1 < numWords && word2 < numWords && word3 < numWords;
}

const string ThreeWordCodeSource::nextCode() {
	buffer.clear();
	buffer += words[word1];
	buffer += ' ';
	buffer += words[word2];
	buffer += ' ';
	buffer += words[word3];

	const size_t maxWord = numWords-1;
	word1++;
	if (word1 > maxWord) {
		word1 = 0;
		word2++;
		if (word2 > maxWord) {
			word2 = 0;
			word3++;
			// no need to check last word, since hasNext will be false when it gets too big
		}
	}
	return buffer;
}

void ThreeWordCodeSource::reset()
{	
	word1 = 0;
	word2 = 0;
	word3 = 0;
}

bigcount_t ThreeWordCodeSource::size() const {
	return numWords * numWords * numWords;
}

size_t ThreeWordCodeSource::width() const {
	return words.width*3 + 2;
}

ostream& ThreeWordCodeSource::print(ostream& os) const {
	os << "ThreeWordCodeSource with " << numWords << " words, size=" << size();
	return os;
}

ostream& PresetGreekCodeSource::print(ostream& os) const {
	os << "Three greek letter names (" << numWords << " choices), size=" << size();
	return os;
}

ostream& PresetDictCodeSource::print(ostream& os) const {
	os << "Three dictionary words (" << numWords << " choices), size=" << size();
	return os;
}




/*************************************
          MultiWordCodeSource
**************************************/

const string MultiWordCodeSource::DEFAULT_SEPARATOR(" ");
// Make an empty MultiWordCodeSource. Use addWord to add content.
MultiWordCodeSource::MultiWordCodeSource()
{
	all_done = false;
	max_length = 0;
	num_words = 0;
}
// Initialize from the given lists. If the separators is too short, DEFAULT_SEPARATOR will be used.
// If it is too long, the extra entries will be silently ignored.
// There should be one more separator than word.
MultiWordCodeSource::MultiWordCodeSource(const vector<WordList>& words, const vector<string>& separators)
	: words(words), num_words(words.size()), indices(words.size(), 0)
{
	all_done = false;
	max_length = 0;
	for (size_t i = 0; i < num_words+1; i++) {
		if (i < separators.size())
			this->separators.push_back(separators[i]);
		else
			this->separators.push_back(DEFAULT_SEPARATOR);
		max_length += separators[i].length();
		if (i < num_words) {
			max_length += words[i].length;
		}
	}
}
// Add a word to the code source. The prefix text will be used to separate this word
// from the word before it. Do not add words once you start iterating through codes.
void MultiWordCodeSource::addWord(const WordList& wordList, string prefixText) {
	assert(wordList.length > 0);
	words.push_back(wordList);
	++num_words;
	indices.push_back(0);
	separators.push_back(prefixText);
	max_length += wordList.length;
	max_length += prefixText.length();
}
// Add the last text fragment, if needed, to make sure there is exactly one
// more separator than word list.
void MultiWordCodeSource::finalize(string lastText) {
	if (separators.size() < num_words + 1) {
		separators.push_back(lastText);
		max_length += lastText.length();
	}
}
bool MultiWordCodeSource::hasNext() const
{
	return !all_done;
}
const string MultiWordCodeSource::nextCode()
{
	assert(!all_done);
	// join current words from each source
	string buffer;
	buffer.reserve(max_length);
	buffer += separators[0];
	for (size_t i = 0; i < num_words; i++) {
		buffer += words[i][indices[i]];
		buffer += separators[i+1];
	}

	// increment the first source, bubbling up as needed
	for (size_t i = 0; i < num_words; i++) {
		++indices[i];
		if (indices[i] < words[i].length) {
			break;
		} else {
			// at end of word list
			if (i == num_words-1) {
				// on the last source
				// don't reset it, just mark the sequence as done
				all_done = true;
				break;
			}
			// restart at the beginning
			indices[i] = 0;
			// ... go on to the next source, incrementing it
		}
	}
	return buffer;
}

void MultiWordCodeSource::reset()
{
	fill(indices.begin(), indices.end(), 0);
	all_done = false;
}

bigcount_t MultiWordCodeSource::size() const {
	bigcount_t product = 1;
	BOOST_FOREACH(const WordList& l, words) {
		product *= l.length;
	}
	return product;
}

size_t MultiWordCodeSource::width() const {
	return max_length;
}

ostream& MultiWordCodeSource::print(ostream& os) const {
	os << "Pattern of " << words.size() << " sources: `";
	size_t i;
	for (i = 0; i < words.size(); i++) {
		os << separators[i] << "(word" << i+1 << ")";
	}
	os << separators[i] << "', ";
	os << "size=" << size();
	return os;
}




/*************************************
            CodeSourceList
**************************************/

// Make an empty list of sources. Use addSource or addSourceCopy to add more sources.
CodeSourceList::CodeSourceList() {
	sourceNum = 0;
}

// Add the given source. The list TAKES OWNERSHIP of the pointer.
// Please ensure the object pointed to does not go out of scope.
void CodeSourceList::addSource(AbstractCodeSource* source) {
	sources.push_back(source);
}
// Add the given source by cloning it. You are free to do whatever you want
// with the pointer afterwards.
void CodeSourceList::addSourceCopy(const AbstractCodeSource* source) {
	sources.push_back(source->clone());
}

bool CodeSourceList::hasNext() const {
	return sourceNum < sources.size() && sources[sourceNum].hasNext();
}

const string CodeSourceList::nextCode() {
	// grab the current return value
	const string ret = sources[sourceNum].nextCode();
	// advance to next. If no next, move on to the next source in the list that has one
	while (sourceNum < sources.size() && !sources[sourceNum].hasNext())
		sourceNum++;
	return ret;
}

void CodeSourceList::reset() {
	BOOST_FOREACH(AbstractCodeSource& a, sources)
		a.reset();
	sourceNum = 0;
}

bigcount_t CodeSourceList::size() const {
	bigcount_t sum = 0;
	BOOST_FOREACH(const AbstractCodeSource& a, sources)
		sum += a.size();
	return sum;
}

size_t CodeSourceList::width() const {
	size_t biggest_width = 0;
	BOOST_FOREACH(const AbstractCodeSource& a, sources)
		biggest_width = max(biggest_width, a.width());
	return biggest_width;
}

double CodeSourceList::avg_width() const {
	double accum = 0;
	BOOST_FOREACH(const AbstractCodeSource& a, sources)
		accum += a.width() * a.size();
	return accum / size();
}

ostream& CodeSourceList::print(ostream& os) const {
	os << "Sequence of " << sources.size() << " code sources: [\n";
	BOOST_FOREACH(const AbstractCodeSource& a, sources)
		os << "-\t" << a << "\n";
	os << "] for a total size of " << size();
	return os;
}
