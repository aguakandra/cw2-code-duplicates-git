#pragma once
#include "SearchSpace.h"
#include <fstream>
#include <string>
#include <vector>
#include "Seed.h"

/**
This class stores code/seed pairs to a text file.
Optionally, split the file into many pieces based on a limit of records per file.
Records are stored as the seed (8 hex chars), the code, then LINE_SEP_CHAR.
*/
class CodeRecorder
{
public:
	static const size_t DEFAULT_NUM_FILES = 128;
	static const size_t DEFAULT_RECORDS_PER_FILE = SearchSpace::SIZE / DEFAULT_NUM_FILES;
	static const size_t ONE_FILE = 0; // zero recordsPerFile means don't split the file

	// The character separating one seed/code pair from another.
	static const char LINE_SEP_CHAR = '\n';
	
	// Make a new code recorder based on the given filename.
	// If the recordsPerFile is not zero (ONE_FILE), then the data will be split into files
	//     of recordsPerFile records. The files will be named as filename0, filename1, ...
	// If the recordsPerFile is zero, then the data will be written to one file named filename
	// Postcondition: the first file will be open and saved in currentFile.
	CodeRecorder(std::string filename, size_t recordsPerFile = DEFAULT_RECORDS_PER_FILE);

	// Record the code/seed pair in the file. Maybe go on to the next file if this one is full.
	// Returns true if the current file changed.
	bool recordCode(const std::string& code, seed_t seed);
	// Ensure the currently open file is closed.
	void close();
	// accessors for fileNumber
	size_t getFileNumber() const {return fileNumber;}
	void setFileNumber(size_t fn) {fileNumber = fn;}
	// get a list of all files generated. Will be at least one.
	std::vector<std::string> getFileList();

private:
	// the filename string passed to the constructor
	std::string filenamePrefix;
	// for split files, filenamePrefix + fileNumber. For one files, just filenamePrefix.
	std::string currentFileName;
	// for split files, the current file number starting from zero. For one files, always 0.
	size_t fileNumber;
	// the records per file parameter passed to the constructor
	size_t recordsPerFile;
	// the count of records in the current file
	size_t recordsInCurrentFile;
	// Generate currentFileName from info. Will be filenamePrefix for one file,
	// filenamePrefix + fileNumber for split files. Does not advance fileNumber.
	void formFileName();
	// open currentFileName. Does not change fileNumber etc.
	// resets recordsInCurrentFile to zero.
	void openFile();
	// access to the current file
	std::ofstream filestream;
};

