#pragma once


#include <cstdint>

typedef uint32_t seed_t;
typedef uint64_t bigcount_t;
