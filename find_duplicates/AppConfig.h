#pragma once
#include "AbstractCodeSource.h"
#include "CodeRecorder.h"
#include <string>

#define VERSION  "v0.1"
#define DATE     "6/12/2013"
#define AUTHOR   "Paul Holcomb (aguakandra@gmail.com)"
#define SYNOPSIS "Search through CW2 codes for duplicates (two codes that hash to the same seed)"

/*
This class parses the command line arguments to determine how the program should be run.
It stores data about that, including a mode.
The mode can be:
- help displayed (so now you can exit)
- valid config (and here's the list of code sources)
- invalid config (error was displayed, so now you can exit)
These extra values can be specified on the command line:
- Status update interval in seconds
- maximum # of codes per file for temp files
- result filename
*/
class AppConfig {
public:
	// default statusIntervalSecs
	static const size_t DEFAULT_STATUS_INTERVAL = 20;
	// default records per file = don't split files
	static const size_t DEFAULT_RECORDS_PER_FILE = CodeRecorder::ONE_FILE;
	// default output file
	static const std::string DEFAULT_OUTPUT_FILE;
	// result type of parsing the app config
	enum Mode {HELP_DISPLAYED, VALID_CONFIG, INVALID_CONFIG};

	Mode getMode() const {return mode;}
	size_t getStatusInterval() const {return statusIntervalSecs;}
	size_t getRecordsPerFile() const {return recordsPerFile;}
	std::string getOutputFile() const {return outputFile;}
	CodeSourceList getCodeSources() const {return codeSources;}

	// Entry point to constructing an AppConfig from command line arguments.
	static AppConfig parseCommandLine(int argc, char** argv);

private:
	Mode mode;
	size_t statusIntervalSecs;
	size_t recordsPerFile;
	std::string outputFile;
	CodeSourceList codeSources;
	AppConfig()
		: mode(INVALID_CONFIG),
		statusIntervalSecs(DEFAULT_STATUS_INTERVAL),
		recordsPerFile(DEFAULT_RECORDS_PER_FILE)
	{}
};
