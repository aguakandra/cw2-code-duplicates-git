#include "Util.h"
using namespace std;

bool fexists(string filename)
{
  ifstream ifile(filename);
  return !!ifile;
}

RunningAverage::RunningAverage(int history_length) : HISTORY_LENGTH(history_length) {
    values = new DATA[HISTORY_LENGTH];
    //cout << "dynamic array has " << HISTORY_LENGTH << " elements at " << __FILE__ << " line " << __LINE__ << endl;
    index = 0;
    num_values = 0;
}

ostream& displayByteSize(ostream& os, bigcount_t bytes) {
	static const char* prefixes[] = {"", "kilo", "mega", "giga"};
	static const size_t N_prefixes = sizeof(prefixes) / sizeof(prefixes[0]);
	const bigcount_t threshold = 2048; // don't go to bigger unit until more than this many
	const bigcount_t shift_by = 10; // divide by 1024

	size_t prefix_index = 0;
	while (bytes > threshold && prefix_index < N_prefixes-1) {
		// bytes is big and we can reduce it and go on to the next prefix
		bytes >>= shift_by;
		prefix_index++;
	}
	os << bytes << " " << prefixes[prefix_index] << "bytes";
	
	return os;
}



void RunningAverage::record(DATA value) {
    values[index] = value;
    
    index++;
    index %= HISTORY_LENGTH;
    
    num_values = min(num_values + 1, HISTORY_LENGTH);
}


DATA RunningAverage::getAverageValue() const {
    DATA total = 0;
    for (int i = 0; i < num_values; i++) {
        total += values[i];
    }
    return num_values == 0 ? (DATA)0 : total / num_values;
}


void RunningAverage::clear() {
    for (int i = 0; i < HISTORY_LENGTH; i++) {
        values[i] = 0;
    }
}


RunningAverage::~RunningAverage() {
    delete [] values;
}
    
/*
SimulationETA::SimulationETA(int runningAverageDepth, int numberOfTests)
    : RUNNING_AVERAGE_DEPTH(runningAverageDepth), averageSecondsPerTest(runningAverageDepth)
{
    totalTests = numberOfTests;
    completedTests = 0;
    
    ETA = 0;
}

void SimulationETA::testStarted() {
    testStartClock = clock();
}

void SimulationETA::testStopped() {
    testEndClock = clock();
    completedTests++;
    
    float seconds = ((float) testEndClock - testStartClock) / CLOCKS_PER_SEC;
    
    averageSecondsPerTest.record(seconds);
    
    //cout << (totalTests - completedTests) << " remain" << endl;
    secondsRemaining = (int) averageSecondsPerTest.getAverageValue();
    if (completedTests <= totalTests) secondsRemaining *= (totalTests - completedTests);
    else secondsRemaining *= 1; // *= next_fibonacci(completedTests - totalTests); TODO: implement this

    time_t now;
    time(&now);
    ETA = now + secondsRemaining;
}

int SimulationETA::getTotalNumberOfTests() const {
    return totalTests;
}

void SimulationETA::setTotalNumberOfTests(int numberOfTests) {
    totalTests = numberOfTests;
}

float SimulationETA::getPercentDone() const {
    return ((float) completedTests) / totalTests * 100;
}

float SimulationETA::getSecondsPerTest() const {
    return (float) averageSecondsPerTest.getAverageValue();
}

time_t SimulationETA::getETA() const {
    return ETA;
}

void SimulationETA::reset() {
    completedTests = 0;
    averageSecondsPerTest.clear();
    ETA = 0;
}

ostream& operator<<(ostream& os, const SimulationETA& s) {
    os << s.getPercentDone() << "% done; at " << s.getSecondsPerTest() << " sec/test ~"
       << s.secondsRemaining << " sec left; ETA " << ctime(&s.ETA);
    return os;
}
*/