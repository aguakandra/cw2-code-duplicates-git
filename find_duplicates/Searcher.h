#pragma once
#include "AbstractCodeSource.h"
#include "Seed.h"
#include "SearchSpace.h"
#include "CodeRecorder.h"
#include <process.h>
#include <Windows.h>
#include "Util.h"

/**
The searcher attempts to find a code for every seed. 
It uses an AbstractCodeSource to generate the codes and a SearchSpace to record which seeds have been found.
When a code is found for a new seed, the code is written to a file and the seed is marked as found.
A CodeRecorder handles this process. By default it splits the file up into pieces.
Since this is a very lengthy process (~1 day), the searcher is able to save state:
It can stop, save state, then resume where it left off.
The IO module provides those functions.
The search is performed in a worker thread, which this class has control over.
*/
class Searcher : public noncopyable
{
public:
	Searcher(AbstractCodeSource& codeSource, CodeRecorder& codeRecorder);

	// Tell the searcher to start and wait for it to be started.
	void start();
	// Tell the searcher to stop and wait for it to be stopped.
	void stop();
	// Block forever until the searcher thread exits (due to completion or code exhaustion)
	void waitForStop();

	// get status
	bigcount_t getAttemptedCodes() const;
	bigcount_t getFoundSeeds() const;

	// read/write state to a file (implemented in IO.cpp)
	void writeState(const char * filename);
	void readState(const char * filename);

	static const int ANNOUNCE_EVERY = 0x800000; // spew a progress message every this many attempted codes
	static const bool ANNOUNCE = true;

private:
	// Static thread entry point, since we can't create a thread to an instance method.
	static unsigned __stdcall threadEntryStatic(void* pThis);
	// Main search function
	void doSearch();
	void makeThread();

	bool running;
	bigcount_t attemptedCodes;
	HANDLE hThread;

	AbstractCodeSource& codeSource;
	SearchSpace searchSpace;
	CodeRecorder& codeRecorder;
};
