#include "AppConfig.h"
#include <iostream>
#include <vector>
#include <string>
#include <boost/program_options.hpp>
#include <boost/regex.hpp>
#include <boost/foreach.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/lexical_cast.hpp>

namespace po = boost::program_options;
using namespace std;

const string AppConfig::DEFAULT_OUTPUT_FILE("duplicates.txt");


/***************************************
          Exception Classes
***************************************/

// Can't open word list file
class InputFailure : virtual public std::exception {
	string _what;
public:
	InputFailure(const string& file)
		: _what("Can't open word list '" + file + "' as input.")
	{}
	virtual const char * what() const throw() {return _what.c_str();}
};
class InvalidWordPatternSpec : virtual public std::exception {
	string _what;
public:
	InvalidWordPatternSpec(const string& pattern, size_t bad_num)
		: _what("Invalid pattern spec '" + pattern + "' since source number " + 
		boost::lexical_cast<string>(bad_num) + " is out of range.")
	{}
	virtual const char * what() const throw() {return _what.c_str();}
};


/***************************************
           Code Source makers
***************************************/
// abstract class representing the ability to construct an AbstractCodeSource
// and add it to a CodeSourceList
class CodeSourceSpec {
public:
	virtual void addTo(CodeSourceList& container) = 0;
};
// Preset Sources : 3greeks, 3dicts, numbers
class PresetSourceSpec : public CodeSourceSpec {
public:
	void addTo(CodeSourceList& container) {
		container.addSource(new PresetGreekCodeSource());
		container.addSource(new PresetDictCodeSource());
		container.addSource(new NumberCodeSource());
	}
};
// Dates (default range from 1/1/0 to 12/31/9999)
class DateSourceSpec : public CodeSourceSpec {
public:
	void addTo(CodeSourceList& container) {
		container.addSource(new DateCodeSource());
	}
};
// SequentialLettersCodeSource(N)
class LetterSourceSpec : public CodeSourceSpec {
public:
	const int nLetters;
	LetterSourceSpec(int nLetters) : nLetters(nLetters) {}
	LetterSourceSpec(string s) : nLetters(boost::lexical_cast<int,string>(s)) {}
	void addTo(CodeSourceList& container) {
		container.addSource(new SequentialLettersCodeSource(nLetters));
	}
};
// WordPattern - parse a pattern, split into word list references and separator text.
// When linked to the WordLists, generate a MultiWordCodeSource.
class WordPatternSpec : public CodeSourceSpec {
	// ex: if the pattern is a 1 b 2 c 3 d, this holds "a ", " b ", " c ", " d"
	// ex: if the pattern is 131, this holds "","","",""
	// the length is one more than the number of sources; sources are not assumed to be unique here.
	vector<string> component_separators;
	// holds the parsed numbers
	vector<size_t> component_numbers;
	boost::shared_ptr<vector<WordList> > wordListRef;
public:
	WordPatternSpec(string pattern, size_t n_word_files);
	void mark_used_files(set<int>& used_files);
	void useWordLists(boost::shared_ptr<vector<WordList> > wordLists) {wordListRef = wordLists;}
	void addTo(CodeSourceList& container) {
		vector<WordList> usedWordLists;
		BOOST_FOREACH(size_t src_num, component_numbers)
			usedWordLists.push_back((*wordListRef)[src_num-1]); // transition from 1-based pattern to 0-based data
		container.addSource(new MultiWordCodeSource(usedWordLists, component_separators));
	}
};
// Parse the pattern and store the strings and numbers in the component vectors
// Postcondition: there will be exactly one more separator than number component.
WordPatternSpec::WordPatternSpec(string pattern, size_t n_word_files) {
	static const boost::regex r("(\\d)|\\((\\d+)\\)"); // a single digit or (many digits), saving the digits.
	component_separators.clear();
	component_numbers.clear();
	// Iterate through the pattern twice, on the matches and on the stuff in between, the tokens.
	boost::sregex_token_iterator tok_it(pattern.begin(), pattern.end(), r, -1), tok_end;
	boost::sregex_iterator match_it(pattern.begin(), pattern.end(), r), match_end;
	while (tok_it != tok_end) {
		// add the text between matches as the next separator
		component_separators.push_back(*tok_it);
		tok_it++;
		// if there was a match (b/c this is not the last token),
		// add the matched integer as the next number
		if (match_it != match_end) {
			// rename for convenience
			const boost::sregex_iterator::value_type& mr = *match_it;
			size_t num = boost::lexical_cast<size_t>(mr[1].matched ? mr[1] : mr[2]);
			// validate number range
			if (num < 1 || num > n_word_files+1)
				throw InvalidWordPatternSpec(pattern, num);
			component_numbers.push_back(num);
			match_it++;
		}
	}
	// if the pattern ended with a match, add an empty separator for the end.
	if (component_numbers.size() == component_separators.size()) {
		component_separators.push_back("");
	}
}
void WordPatternSpec::mark_used_files(set<int>& used_files) {
	BOOST_FOREACH(int num, component_numbers)
		used_files.insert(num);
}




/***************************************
          Command Line Parser
***************************************/
AppConfig AppConfig::parseCommandLine(int argc, char **argv)
{
	AppConfig config;	
    try {
		// Yes, this could be made static or something. But this function should only be called once.

		//// Make the ProgramOptions config objects ////
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "Show this help message.\n")
			("word-file,f", po::value< vector<string> >(), "Use words from file [arg], one word per line. Can be specified multiple times.\n")
			("output-file,o", po::value<string>(&config.outputFile)->default_value(DEFAULT_OUTPUT_FILE),
				"Write final output to this file.\n")
			("update,u", po::value<size_t>(&config.statusIntervalSecs)->default_value(DEFAULT_STATUS_INTERVAL),
				"Show status updates every [arg] seconds.\n")
			("records,r", po::value<size_t>(&config.recordsPerFile)->default_value(DEFAULT_RECORDS_PER_FILE),
				"Split temp files after [arg] records.\n0 means never split.\n")
		;
		po::options_description code_opts_desc("Code Sources");
		code_opts_desc.add_options()
			("presets,p", "Use the codes generated by the Random Mission button "
			              "(three words from a list of 100, three greek letters, or a number 0-9999).\n")
			("dates,d", "Use codes generated by Today's Mission button (all dates from Jan 1, 0 to Dec 31, 9999).\n")
			("letters,l", po::value<int>(), "Combinations of up to [arg] lowercase letters.\n")
            ("word,w", po::value< vector<string> >(),
				"A phrase formed from one or more words, from one or more word files. "
			    "This can be any string, where numbers will be replaced by a word from the respective word list.\n\n"
				"Number can be in the range 1-N, where N is the number of word files specified, "
				"and can be wrapped in parentheses for clarity.\n\n"
				"Examples:\n-w 123 \tconcatenates a word from lists 1, 2, and 3.\n"
				"-w \"1 1\" \tjoins two words from the first word file using a space.\n"
				"-w \"The (5) of (4)s\" \tinserts words into a string like The Two of Clubs.\n")
        ;
		desc.add(code_opts_desc);

		//// Parse the command line, saving the raw parse options for later use ////
        po::variables_map vm;
		po::parsed_options& parsed = po::parse_command_line(argc, argv, desc);
        po::store(parsed, vm);
        po::notify(vm);
		
		//// If help is requested, display it and be done. ////
        if (vm.count("help")) {
			cout << argv[0] << " " << VERSION << " (" << DATE << ")\n";
			cout << "Created by " << AUTHOR << "\n\n";
			cout << SYNOPSIS << "\n\n";
            cout << desc << "\n";
			cout << "Codes are generated from -r, -d, -l, and -w options in the order given. Only -w can be specified more than once.\n"
				 << "At least one code source option is required.\n"
				 << "For example,\n"
				 << "-r -l 4        searches all preset codes, then from a to zzzz\n"
				 << "-w 1 -w \"2 2\"  searches single words in file 1, then two-word phrases in file 2\n\n"
				 ;
			
			config.mode = Mode::HELP_DISPLAYED;
			return config;
        }

		//// Make our own copy of the word files ////
		vector<string> word_files;
		if (vm.count("word-file")) {
			const vector<string>& w = vm["word-file"].as< vector<string> >();
			copy(w.begin(), w.end(), back_inserter<vector<string> >(word_files));
        }
		const size_t n_word_files = word_files.size();

		//// Build up a list of code sources in the order given ////
		// If there is an error, the SourceSpec constructor will throw.
		boost::ptr_vector<CodeSourceSpec> codeSourceSpecs;
		BOOST_FOREACH(po::option o, parsed.options) {
			const bool is_code = code_opts_desc.find_nothrow(o.string_key, false) != NULL;
			if (is_code) {
				if (o.string_key == "presets")
					codeSourceSpecs.push_back(new PresetSourceSpec());
				else if (o.string_key == "dates")
					codeSourceSpecs.push_back(new DateSourceSpec());
				else if (o.string_key == "letters")
					codeSourceSpecs.push_back(new LetterSourceSpec(o.value[0]));
				else if (o.string_key == "word")
					codeSourceSpecs.push_back(new WordPatternSpec(o.value[0], n_word_files));
			}
		}

		//// Read in WordLists from file arguments ////
		// Determine which word files were actually referenced
		set<int> used_files;
		BOOST_FOREACH(CodeSourceSpec& css, codeSourceSpecs) {
			if (WordPatternSpec* wps = dynamic_cast<WordPatternSpec*>(&css))
				wps->mark_used_files(used_files);
		}
		// Read those files in.
		// the wordLists is a shared pointer for easy sharing with the WordPatternSpecs
		boost::shared_ptr<vector<WordList> > wordLists = boost::make_shared<vector<WordList> >();
		const set<int>::iterator used_files_end = used_files.end();
		for (size_t i = 1; i <= n_word_files; i++) {
			if (used_files.find(i) != used_files_end) {
				ifstream fin(word_files[i-1]);
				if (!fin) throw InputFailure(word_files[i-1]);
				wordLists->push_back(WordList::readFile(fin));
				fin.close();
			}
		}

		//// Link WordLists with WordPatternSpecs ////
		BOOST_FOREACH(CodeSourceSpec& css, codeSourceSpecs) {
			if (WordPatternSpec* wps = dynamic_cast<WordPatternSpec*>(&css))
				wps->useWordLists(wordLists);
		}

		//// All ready to use the CodeSourceSpecs to generate the code sources and add to the list ////
		BOOST_FOREACH(CodeSourceSpec& css, codeSourceSpecs) {
			css.addTo(config.codeSources);
		}
		// TODO: handle case where there are no code sources, maybe display help or look for save file.

		config.mode = Mode::VALID_CONFIG;
    }
    catch(exception& e) {
        cerr << "error: " << e.what() << "\n";
		config.mode = Mode::INVALID_CONFIG;
		return config;
    }


	return config;
}
