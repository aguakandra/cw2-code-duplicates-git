#include "CodeRecorder.h"
#include <iostream>
#include <iomanip>
#include <boost/lexical_cast.hpp>
using namespace std;


// Make a new code recorder based on the given filename.
// If the recordsPerFile is not zero (ONE_FILE), then the data will be split into files
//     of recordsPerFile records. The files will be named as filename0, filename1, ...
// If the recordsPerFile is zero, then the data will be written to one file named filename
// Postcondition: the first file will be open and saved in currentFile.
CodeRecorder::CodeRecorder(string filename, size_t recordsPerFile) 
	: filenamePrefix(filename), recordsPerFile(recordsPerFile), fileNumber(0)
{
	// Turn on auto-exception throwing for the file stream
	// A failure (in opening file, formatting a value) will throw an exception
	// A system failure (like no extra space available) will throw an exepction
	filestream.exceptions(iostream::badbit | iostream::failbit);

	// generate the file name and open the first file
	// (recordsInCurrentFile will be set to zero there)
	formFileName();
	openFile();
}

// Record the code/seed pair in the file. Maybe go on to the next file if this one is full.
// Returns true if the current file changed.
bool CodeRecorder::recordCode(const string& code, seed_t seed) {
	// format is like 004488CCMary had a Little Lamb|12345678Another Code ...
	// always starting with 8 hex digits
	// where '|' is the line sep char
	filestream << hex << setw(8) << seed << dec;
	filestream << code << LINE_SEP_CHAR;
	++recordsInCurrentFile;

	// maybe advance files
	if (recordsPerFile != ONE_FILE && recordsInCurrentFile >= recordsPerFile) {
		cout << "--- going to next file since there are " << recordsInCurrentFile << " records in this one.\n";
		close();
		++fileNumber;
		formFileName();
		openFile();
		return true;
	}
	return false;
}

// Ensure the currently open file is closed.
void CodeRecorder::close() {
	if (filestream.is_open())
		filestream.close();
}

// Generate currentFileName from info. Will be filenamePrefix for one file,
// filenamePrefix + fileNumber for split files. Does not advance fileNumber.
void CodeRecorder::formFileName() {
	currentFileName = filenamePrefix;
	if (recordsPerFile)
		currentFileName += boost::lexical_cast<string>(fileNumber);
}

// open the file for the current info. Does not change fileNumber etc.
// resets recordsInCurrentFile to zero.
void CodeRecorder::openFile() {
	filestream.open(currentFileName.c_str(), fstream::trunc);
	filestream.fill('0'); // for hex seeds
	recordsInCurrentFile = 0;
	cout << "--- writing to " << currentFileName << "\n";
}


// get a list of all files generated. Will be at least one.
vector<string> CodeRecorder::getFileList() {
	vector<string> fileNames;
	size_t old_file_num = fileNumber;
	// simulate a run through all the files
	for (fileNumber = 0; fileNumber <= old_file_num; fileNumber++) {
		formFileName();
		fileNames.push_back(currentFileName);
	}
	// restore old
	fileNumber = old_file_num;
	formFileName();

	return fileNames;
}