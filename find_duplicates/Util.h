
#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib> // for system, exit
#include "Seed.h"

class noncopyable
{
protected:
    noncopyable() {}
    ~noncopyable() {}
private:  // emphasize the following members are private
    noncopyable( const noncopyable& );
    const noncopyable& operator=( const noncopyable& );
};

inline void die(std::string msg="") {
    std::cerr << msg << std::endl;
    system("PAUSE");
    exit(1);
}
bool fexists(std::string filename);

// display size in bytes, like 300 megabytes
std::ostream& displayByteSize(std::ostream& os, bigcount_t bytes);

// TODO: see if any of these utilities are still useful

// running average of a given number of float values
// call record(value) to record each value and call getAverageValue() to get the running average
typedef double DATA;
class RunningAverage : public noncopyable {
public:
    RunningAverage(int history_length = 10);
    ~RunningAverage();
    
    const int HISTORY_LENGTH;
    
    void record(DATA value);
    
    DATA getAverageValue() const;
    
    void clear();
private:
    DATA* values;
    int index;
    int num_values;
};
    
/*
// estimate the completion time of a simulation involving many tests / steps
// (uses a running average of configurable depth)
class SimulationETA {
public:
    const int RUNNING_AVERAGE_DEPTH;
    SimulationETA(int runningAverageDepth = 10, int numberOfTests = 100);
    
    void testStarted();
    void testStopped();
    
    int getTotalNumberOfTests() const;
    void setTotalNumberOfTests(int numberOfTests);
    
    float getPercentDone() const;
    float getSecondsPerTest() const;
    time_t getETA() const;
    
    void reset();
    
    friend ostream& operator<<(ostream& os, const SimulationETA& s);
private:
    clock_t testStartClock, testEndClock;
    time_t ETA;
    
    int totalTests, completedTests;
    
    int secondsRemaining;
    
    RunningAverage averageSecondsPerTest;
};
*/
