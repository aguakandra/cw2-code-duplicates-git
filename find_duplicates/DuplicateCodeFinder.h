#pragma once
#include "Seed.h"
#include "SearchSpace.h"
#include "CodeRecorder.h"
#include <string>
#include <map>
#include <set>

// Look for duplicates in an incoming stream of code/seed pairs.
// Log all codes to a file and maintain a record of seeds seen.
// When a duplicate is found, log that code to a separate file.
// In the end, read through the two files to get a full list of
// duplicated seeds and their codes
class DuplicateCodeFinder
{
private:
	SearchSpace seeds_seen;
	CodeRecorder codeRecorder, duplicateRecorder;
	std::set<seed_t> seeds_with_duplicates;
	/*
	I'm using the full 2^32 bitmask for recording whether a seed has been seen before
	and a smaller list of just the seeds with duplicates
		I'm assuming this will be small
		and the main operation on it is to print out a list of those seeds that have 1 or more duplicates
		so I don't need the another large bitmask.
		TODO: test performance of this class
	*/
public:
	DuplicateCodeFinder(const std::string& codefile = DEFAULT_CODEFILE, const std::string& dupfile = DEFAULT_DUPFILE, size_t recordsPerFile = CodeRecorder::ONE_FILE);
	
	static const std::string DEFAULT_CODEFILE, DEFAULT_DUPFILE, DEFAULT_RESULTFILE;
	const std::string codefile, dupfile, resultfile;
	
	/*
	Accumulate code/seed pairs. Store everything in the main codeRecorder file,
	and store duplicates, etc in the second file. We need to remember all codes
	since we don't know which codes will produce duplicates and we might need the
	first code later.
	*/
	void addCode(const seed_t seed, const std::string& code);
	const std::set<seed_t>& getDuplicateSeeds() const;
	bigcount_t numSeeds() const {return seeds_with_duplicates.size();}
	// flush the open files and close them.
	void closeFiles();
	/*
	Go through the two intermediate files and make a new result file,
	listing each seed that is duplicated and all codes corresponding to that seed.
	*/
	void compileResults(const std::string& resultfile = DEFAULT_RESULTFILE);
	// Remove intermediate files
	void cleanup();
};

