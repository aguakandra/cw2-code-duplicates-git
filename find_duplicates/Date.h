#pragma once
#include <iostream>
class Date
{
private:
	static int days_in_month(int year, int month);
	static bool is_leap_year(int year);
public:
	Date(int year = 0, int month = 1, int day = 1);  
	
	bool operator==(const Date& rhs) const {
		return year == rhs.year && month == rhs.month && day == rhs.day;
	}
	bool operator<(const Date& rhs) const {
		return year < rhs.year
			|| (year == rhs.year
				&& (month < rhs.month
					|| month == rhs.month
						&& day < rhs.day
					)
				);
	}
	bool operator<=(const Date& rhs) const {
		return *this < rhs || *this == rhs;
	}
	friend std::ostream& operator<<(std::ostream& os, const Date& d);
	static int days_between(Date a, Date b);
	//Date& operator++();

	Date next() const;

	int year;// like 2020 or 99999 or 0
	int month; // 1 = jan, 12 = dec
	int day; // 1 .. 31
};
