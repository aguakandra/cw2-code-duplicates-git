#include "Searcher.h"
#include "HashAString.h"
#include "Util.h"
#include <fstream>
#include <ctime>
#include <cassert>
#include <cstring>
#include <iomanip>
using namespace std;

Searcher::Searcher(AbstractCodeSource& codeSource, CodeRecorder& codeRecorder)
	: codeSource(codeSource), codeRecorder(codeRecorder), searchSpace(), running(false)
{
	hThread = NULL;
	attemptedCodes = 0;
}

// Make thread to run this->doSearch() via the static entry point
void Searcher::makeThread()
{
	//cout << "creating thread" << endl;
	hThread = (HANDLE) _beginthreadex(
		NULL,                         // security
		0,                            // stack size
		Searcher::threadEntryStatic,  // entry-point-function
        this,                         // arg list; we'll use the "this" pointer
        CREATE_SUSPENDED,             // so we can later call ResumeThread()
        NULL                          // thread id
	);
}

bigcount_t Searcher::getAttemptedCodes() const
{
	return attemptedCodes;
}

bigcount_t Searcher::getFoundSeeds() const
{
	return searchSpace.getNumFoundSeeds();
}

// Tell the searcher to start and wait for it to be started.
void Searcher::start()
{
	if (!running) {
		//cout << "starting" << endl;
		if (hThread == NULL) makeThread();
		running = true;
		WaitForSingleObject(hThread, 100);
		ResumeThread(hThread);
	}
}

// Tell the searcher to stop and wait for it to be stopped.
void Searcher::stop()
{
	if (running) {
		//cout << "stopping" << endl;
		running = false;
		waitForStop();
	}
}

// Block forever until the searcher thread exits (due to completion or code exhaustion)
void Searcher::waitForStop()
{
	// wait until the thread actually finishes
	cout << "waiting for thread to exit." << endl;
	if (running)
		WaitForSingleObject(hThread, INFINITE);
	//cout << "done waiting." << endl;
	hThread = NULL;
}

// Static thread entry point, since we can't create a thread to an instance method.
unsigned __stdcall Searcher::threadEntryStatic(void* pThis)
{
	Searcher* const searcherThis = (Searcher*) pThis;
	searcherThis->doSearch();
	return 0;
}

// Main search function
void Searcher::doSearch()
{
	cout << "starting code search." << endl;
	clock_t start, end, total_start;
	start = total_start = clock();
	end = 0;
	bigcount_t codeStart = attemptedCodes;
	bigcount_t seedStart = searchSpace.getNumFoundSeeds();


	while (running) {
		if (codeSource.hasNext()) {
			// get next code and hash it
			++attemptedCodes;
			const string code = codeSource.nextCode();
			const seed_t seed = hashAString(code.c_str());

			if (!searchSpace.haveFoundCodeForSeed(seed)) {
				// New seed found, record it.
				searchSpace.markFoundCodeForSeed(seed);
				codeRecorder.recordCode(code, seed);
				if (searchSpace.allDone()) {
					cout << "#####\n#####  A L L   D O N E   Search Space Full ! ! !\n#####" << endl;
					break;
				}
			}
		}
		else {
			cerr << "OUT OF CODES before all seeds found!!!\n";
			break;
		}
		/*
		if (ANNOUNCE && attemptedCodes % ANNOUNCE_EVERY == 0) {
			// announce progress
			end = clock();
			const double secs = (end - start + 0.0) / CLOCKS_PER_SEC;

			const bigcount_t seeds = searchSpace.getNumFoundSeeds();
			const bigcount_t deltaCodes = attemptedCodes - codeStart;
			const bigcount_t deltaSeeds = seeds - seedStart;

			const double testRate = secs == 0 ? 0 : deltaCodes / secs;
			const double seedRate = secs == 0 ? 0 : deltaSeeds / secs;

			const double foundPct = double(deltaSeeds) / deltaCodes * 100.0;
			const double totalPct = double(seeds) / SearchSpace::SIZE * 100.0;

			avgSeedRateHistory.record(seedRate);
			const double avgSeedRate = avgSeedRateHistory.getAverageValue();

			const bigcount_t remaining = SearchSpace::SIZE - seeds;
			const int secs_remaining = avgSeedRate == 0 ? 0 : int(remaining / avgSeedRate);

			time_t now = NULL;
			time(&now);
			time_t ETA = now + secs_remaining;

			char buffer[100];
			ctime_s(buffer, 100, &ETA);
			const size_t bl = strnlen(buffer, 100);
			buffer[bl-1] = 0; // delete last char - the annoying newline

			const streamsize oldp = cout.precision();
			const ios::fmtflags oldf = cout.flags();
			cout << "status: seeds=" << seeds << ", codes=" << attemptedCodes 
				 << " in " << setprecision(1) << fixed << secs << "s (" << int(testRate)
				 << " test/s @" << setprecision(3) << fixed << foundPct << "%)\n";
			cout << setfill('0') << setw(6) << setprecision(3) << fixed << totalPct
				 << "% done, ETA " << buffer << " at " << int(seedRate) << " seeds/s" << endl;
			cout.precision(oldp);
			cout.flags(oldf);

			start = clock();
			codeStart = attemptedCodes;
			seedStart = seeds;
		}*/
	}
	running = false;
	end = clock();
	const double secs = (end - total_start + 0.0) / CLOCKS_PER_SEC;

	codeRecorder.close();

	cout << "Search thread is stopping.\n";
	cout << "Searched " << attemptedCodes << " codes, found " << searchSpace.getNumFoundSeeds()
		 << " seeds in " << secs << "s" << endl;
}
