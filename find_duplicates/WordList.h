#pragma once

#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>
#include <iostream>

// A list of words, which cannot be changed once created.
// Memory is shared among copies.
class WordList {
private:
	typedef const std::vector<std::string> list_type;
	const boost::shared_ptr<list_type> data;
public:
	// Make a WordList from an existing list of words.
	// A defensive copy will be made.
	WordList(const std::vector<std::string>& input_list);
	// Make a WordList by reading in words from a file stream.
	static WordList readFile(std::istream& input_filestream);
	// Access a word from the WordList.
	inline const std::string& operator[](size_t index) {return (*data)[index];}
	// Number of elements in the list.
	const size_t length;
	// Length of the biggest word in characters
	const size_t width;
	friend std::ostream& operator<<(std::ostream& os, const WordList& w);
};
