#include "Date.h"
#include <string>
using namespace std;

bool Date::is_leap_year(int year)
{
	if (year % 4 == 0) {
		if (year % 100 == 0) {
			if (year % 400 == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	} else {
		return false;
	}
}

static const int common_month_sizes[] = {31,28,31,30,31,30,31,31,30,31,30,31};
static const int leap_month_sizes[] =   {31,29,31,30,31,30,31,31,30,31,30,31};
int Date::days_in_month(int year, int month)
{
	if (is_leap_year(year))
		return leap_month_sizes[month-1];
	else
		return common_month_sizes[month-1];
}
Date::Date(int year, int month, int day):year(year), month(month), day(day)
{
}

Date Date::next() const
{
	int month_days = days_in_month(year, month);
	const int year_months = 12;
	int y = year, m = month, d = day;
	++d;
	if (d > month_days) {
		d = 1;
		++m;
		if (m > year_months) {
			m = 1;
			++y;
		}
	}
	return Date(y, m, d);
}

static const string monthNames[] = {
"January",
"February",
"March",
"April",
"May",
"June",
"July",
"August",
"September",
"October",
"November",
"December",
};
static const int num_months = sizeof(monthNames) / sizeof(monthNames[0]); // 12

ostream& operator<<(ostream& os, const Date& d) {
	os << monthNames[d.month-1] << " " << d.day << ", " << d.year;
	return os;
}

// The number of days between a and b, including a but excluding b.
int Date::days_between(Date a, Date b) {
	if (a == b) return 0;
	if (b < a) std::swap(a, b);
	// now a < b

	// Different days in the same month
	if (a.year == b.year && a.month == b.month) {
		return b.day - a.day;
	}
	// Different months in the same year
	if (a.year == b.year) {
		int sum = 0;
		sum += days_in_month(a.year, a.month) - a.day;
		for (int month = a.month+1; month < b.month; month++)
			sum += days_in_month(a.year, month);
		sum += b.day;
		return sum;
	}
	// Different years
	int sum = 0;
	sum += days_between(a, Date(a.year, 12, 31)) + 1; // include endpoint
	sum += days_between(Date(b.year, 1, 1), b); // don't include endpoint
	for (int year = a.year+1; year < b.year; year++)
		sum += is_leap_year(year) ? 366 : 365;
	return sum;
}