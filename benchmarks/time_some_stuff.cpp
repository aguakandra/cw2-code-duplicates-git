#include "AbstractCodeSource.h"
#include "HashAString.h"
#include <boost/timer/timer.hpp>
#include <iostream>
#include <cstdlib>
using namespace std;


double time_code_source(AbstractCodeSource& codeSource, int reps = 1) {
	cout << "Enumerating codes in " << codeSource<< "\n";
	cout << reps << " reps ...\n\n";
	boost::timer::cpu_timer timer;
	bigcount_t num_codes = codeSource.size();
	for (int rep = 0; rep < reps; rep++) {
		codeSource.reset();
		while (codeSource.hasNext()) {
			codeSource.nextCode();
		}
	}
	timer.stop();
	const boost::timer::nanosecond_type elapsed = timer.elapsed().wall;
	const double n = double(num_codes) * reps;
	const double rate = n / double(elapsed) * 1e9;
	cout << timer.format() << "\n";
	cout << n << " codes @ " << rate << " per sec.\n" << endl;
	return rate;
}

double time_md5_from_souce(AbstractCodeSource& codeSource, int reps = 1) {
	cout << "Enumerating and hashing codes in " << codeSource<< "\n";
	cout << reps << " reps ...\n\n";
	boost::timer::cpu_timer timer;
	bigcount_t num_codes = codeSource.size();
	for (int rep = 0; rep < reps; rep++) {
		codeSource.reset();
		while (codeSource.hasNext()) {
			hashAString(codeSource.nextCode().c_str());
		}
	}
	timer.stop();
	const boost::timer::nanosecond_type elapsed = timer.elapsed().wall;
	const double n = double(num_codes) * reps;
	const double rate = n / double(elapsed) * 1e9;
	cout << timer.format() << "\n";
	cout << n << " codes hashed @ " << rate << " per sec.\n" << endl;
	return rate;
}

double time_clone(const AbstractCodeSource& src, int reps = 1) {
	cout << "Cloning " << src << "\n";
	cout << reps << " reps ...\n\n";
	boost::timer::cpu_timer timer;
	for (int rep = 0; rep < reps; rep++) {
		AbstractCodeSource* ptr = src.clone();
		delete ptr;
	}
	timer.stop();
	const boost::timer::nanosecond_type elapsed = timer.elapsed().wall;
	const double n = reps;
	const double rate = n / double(elapsed) * 1e9;
	cout << timer.format() << "\n";
	cout << n << " clones @ " << rate << " per sec.\n" << endl;
	return rate;
}
int main() {
	vector<WordList> words(3, ThreeWordCodeSource::RANDOM_WORDS);
	vector<string> separators(4, "");
	separators[1] = separators[2] = " ";
	MultiWordCodeSource multiDict(words, separators);
	MultiWordCodeSource complicatedMultiWord(vector<WordList>(4, ThreeWordCodeSource::GREEK_WORDS), vector<string>(5, "--=--"));
	CodeSourceList sourceList;
	sourceList.addSource(new PresetDictCodeSource());
	sourceList.addSource(new PresetGreekCodeSource());
	sourceList.addSource(new NumberCodeSource());
	sourceList.addSource(new SequentialLettersCodeSource(4));
	
	//time_code_source(NumberCodeSource(), 4000); // 6.1M/sec
	//time_code_source(PresetDictCodeSource(), 40); // 7.1M/sec
	//time_code_source(PresetGreekCodeSource(), 1000); // 6.5M/sec
	double rateRaw = time_code_source(SequentialLettersCodeSource(5), 10); // 27M/sec

	//cout << "PresetDictCodeSource the hard way:\n";
	//time_code_source(multiDict, 40); // 4.0M/sec

	//cout << "More complicated MultiWordCodeSource:\n";
	//time_code_source(complicatedMultiWord, 40); // 2.8M/sec

	//time_clone(multiDict, 10000000); // 2M/sec
	//time_clone(complicatedMultiWord, 10000000); // 1M/sec

	//time_code_source(sourceList, 40); // 8M/sec

	//double rateMD5 = time_md5_from_souce(SequentialLettersCodeSource(5), 10); // 3.0 M/sec
	//double justHashRate = 1/(1/rateMD5 - 1/rateRaw); // inv of time for MD5+code - time for code = inv of time for just MD5
	//cout << "... So, hash @ " << justHashRate << " per sec.\n" << endl; // 3.3M/sec

	//system("pause");
	return 0;
}
